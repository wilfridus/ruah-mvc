<?php

define('DEBUG', true);

define('DEFAULT_CONTROLLER', 'Home'); //default controller if there isn't one define in the controler
define('DEFAULT_LAYOUT', 'default'); //if no layout is set in the controller use this layout

define('SROOT', '/ruah/'); // set thid to '/' for a live server

define('SITE_TITLE', 'Ruah MVC Framework');
